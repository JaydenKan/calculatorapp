//
//  ViewController.m
//  calculatorApp
//
//  Created by Letstalk on 2019/10/29.
//  Copyright © 2019 JaydenKan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic) BOOL numTyped;
@property (nonatomic) int rowNum;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (nonatomic) NSDecimalNumber  *totalNum;
@property (weak, nonatomic) IBOutlet UILabel *secNumLabel;
@property (nonatomic) NSDecimalNumber *secNum;
@property (weak, nonatomic) IBOutlet UILabel *operationLabel;
@property (weak, nonatomic) IBOutlet UIButton *numButton;
@property (weak, nonatomic) IBOutlet UIButton *operationButton;

//@property (nonatomic) IBOutlet UILabel *selectedRow;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.numTyped = 0;
    self.rowNum = 0;
    

}

- (void)clear{
    
}

- (IBAction)numPressed:(UIButton *)sender
{
    NSString *num = sender.currentTitle;
    //NSLog(@"%@", num);
    UILabel *selectedRow = self.totalLabel;
    if (self.rowNum == 0)
    {
        selectedRow = self.totalLabel;
    }
    else
    {
        selectedRow = self.secNumLabel;
    }
    if ([num isEqualToString:@"0"])
    {
        if(self.numTyped)
        {
            selectedRow.text = [selectedRow.text stringByAppendingString:num];
        }
        else
        {
            if(![selectedRow.text containsString:@"0"]){
                selectedRow.text = @"0";
            }
        }
    }
    else if ([num isEqualToString:@"."])
    {
        if(self.numTyped)
        {
            if(![selectedRow.text containsString:@"."])
                {
                    selectedRow.text = [selectedRow.text stringByAppendingString:num];
                }
        }
        else
        {
            selectedRow.text = [selectedRow.text stringByAppendingString:num];
            self.numTyped = 1;
        }
    }
    else
    {
        if(self.numTyped)
        {
            selectedRow.text = [selectedRow.text stringByAppendingString:num];
        }
        else
        {
            selectedRow.text = num;
            self.numTyped = 1;
        }
    }
}

- (IBAction)clearAll:(id)sender
{
    self.totalNum = [NSDecimalNumber zero];
    self.secNum = [NSDecimalNumber zero];
    self.numTyped = 0;
    self.rowNum = 0;
    self.operationLabel.text = @"";
    self.totalLabel.text = [NSString stringWithFormat:@"%@", self.totalNum];
    self.secNumLabel.text = @"";
    //NSLog(@"%@%@", self.totalNum, self.totalLabel.text);
}

- (IBAction)operationPressed:(UIButton *)sender
{
    //NSLog(@"operationPressed");
    self.operationLabel.text = sender.currentTitle;
    self.numTyped = 0;
    self.rowNum = 1;
}

- (IBAction)ansPressed:(UIButton *)sender
{
    self.totalNum = [NSDecimalNumber decimalNumberWithString:self.totalLabel.text];
    NSLog(@"%@", self.totalNum);
    
    self.secNum = [NSDecimalNumber decimalNumberWithString:self.secNumLabel.text];
    
    if ([self.operationLabel.text isEqualToString:@"+"])
    {
        self.totalNum = [self.totalNum decimalNumberByAdding:self.secNum];
        self.totalLabel.text = [NSString stringWithFormat:@"%@", self.totalNum];
        
        self.secNumLabel.text = @"";
        self.secNum = [NSDecimalNumber zero];
        self.numTyped = 0;
        self.operationLabel.text = @"";
        
    }
    else if ([self.operationLabel.text isEqualToString:@"-"])
    {
        self.totalNum = [self.totalNum decimalNumberBySubtracting:self.secNum];
        self.totalLabel.text = [NSString stringWithFormat:@"%@", self.totalNum];
        
        self.secNumLabel.text = @"";
        self.secNum = [NSDecimalNumber zero];
        self.numTyped = 0;
        self.operationLabel.text = @"";
        
    }
    else if ([self.operationLabel.text isEqualToString:@"*"])
    {
        self.totalNum = [self.totalNum decimalNumberByMultiplyingBy:self.secNum];
        self.totalLabel.text = [NSString stringWithFormat:@"%@", self.totalNum];
        
        self.secNumLabel.text = @"";
        self.secNum = [NSDecimalNumber zero];
        self.numTyped = 0;
        self.operationLabel.text = @"";
        
    }
    else if ([self.operationLabel.text isEqualToString:@"/"])
    {
        self.totalNum = [self.totalNum decimalNumberByDividingBy:self.secNum];
        self.totalLabel.text = [NSString stringWithFormat:@"%@", self.totalNum];
        
        self.secNumLabel.text = @"";
        self.secNum = [NSDecimalNumber zero];
        self.numTyped = 0;
        self.operationLabel.text = @"";
        
    }
}
@end
