//
//  AppDelegate.h
//  calculatorApp
//
//  Created by Letstalk on 2019/10/29.
//  Copyright © 2019 JaydenKan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

